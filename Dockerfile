FROM registry.gitlab.com/dreamer-labs/repoman/dl-kcov-test/image:latest

#TODO remove symlink and make directory rename.
ARG SHUNIT2_VERSION=2.1.7
ARG BASHELLITE_VERSION=1.0.0
ARG SRC_DIR=/src
ARG INSTALL_DIR=/opt

WORKDIR ${SRC_DIR}

ADD https://gitlab.com/dreamer-labs/bashellite/bashellite/-/archive/v${BASHELLITE_VERSION}/bashellite-v${BASHELLITE_VERSION}.tar.gz ${SRC_DIR}/

# The ansible playbook currently requires sudo.
RUN tar xzvf bashellite-v${BASHELLITE_VERSION}.tar.gz && \
    rm bashellite-v${BASHELLITE_VERSION}.tar.gz && \
    yum install -y ansible sudo && \
    chmod +x /src/bashellite-v${BASHELLITE_VERSION}/.init/install-deps.sh && \
    ${SRC_DIR}/bashellite-v${BASHELLITE_VERSION}/.init/install-deps.sh && \
    ansible-playbook ${SRC_DIR}/bashellite-v${BASHELLITE_VERSION}/.test/setup-host.yml

COPY --from=registry.gitlab.com/dreamer-labs/repoman/dl-shunit2-test/image:latest ${INSTALL_DIR}/shunit2 ${INSTALL_DIR}/shunit2


CMD ["/bin/bash"]
